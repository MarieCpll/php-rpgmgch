<!-- Créer une classe User dans User.php qui contient les attributs suivants :

    id (String)
    email (String)
    createdAt (Date) Ajouter les accesseurs nécessaires et rendez ces attributes privés. -->


<?php

class User {

	private $id;
	private $email;
	private $createdAt;

	public function __construct($idUser,$emailUser){
		$this->id = $idUser;
		$this->email = $emailUser;
	}

	public function getIdUser() {
		return $this->id;
	}

	public function getEmailUser() {
		return $this->email;
	}

};

?>